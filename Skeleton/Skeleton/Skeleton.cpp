﻿//=============================================================================================
// Framework for the ray tracing homework
// ---------------------------------------------------------------------------------------------
// Name    : Varga Janos
// Neptun : BHAOHN
//=============================================================================================

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded 
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

const unsigned int windowWidth = 600, windowHeight = 600;

// OpenGL major and minor versions
int majorVersion = 3, minorVersion = 3;

struct vec3 {
	float x, y, z;

	explicit vec3(float x0 = 0, float y0 = 0, float z0 = 0) { x = x0; y = y0; z = z0; }

	vec3 operator*(float a) const { return vec3(x * a, y * a, z * a); }

	vec3 operator+(const vec3& v) const {
		return vec3(x + v.x, y + v.y, z + v.z);
	}
	vec3 operator+(float f) const {
		return vec3(x + f, y + f, z + f);
	}
	vec3 operator-(const vec3& v) const {
		return vec3(x - v.x, y - v.y, z - v.z);
	}
	vec3 operator-(float f) const {
		return vec3(x - f, y - f, z - f);
	}
	vec3 operator*(const vec3& v) const {
		return vec3(x * v.x, y * v.y, z * v.z);
	}
	vec3 operator/(const vec3& v) const {
		return vec3(x / v.x, y / v.y, z / v.z);
	}
	vec3 operator-() const {
		return vec3(-x, -y, -z);
	}
	vec3 normalize() const {
		return (*this) * (1 / (Length() + 0.000001));
	}
	float Length() const { return sqrtf(x * x + y * y + z * z); }

	operator float*() { return &x; }
};

float dot(const vec3& v1, const vec3& v2) {
	return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z);
}

vec3 cross(const vec3& v1, const vec3& v2) {
	return vec3(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
}



void getErrorInfo(unsigned int handle) {
	int logLen;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
	if (logLen > 0) {
		char * log = new char[logLen];
		int written;
		glGetShaderInfoLog(handle, logLen, &written, log);
		printf("Shader log:\n%s", log);
		delete log;
	}
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message) {
	int OK;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if (!OK) {
		printf("%s!\n", message);
		getErrorInfo(shader);
	}
}

// check if shader could be linked
void checkLinking(unsigned int program) {
	int OK;
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if (!OK) {
		printf("Failed to link shader program!\n");
		getErrorInfo(program);
	}
}

// vertex shader in GLSL
const char *vertexSource = R"(
	#version 330
    precision highp float;

	layout(location = 0) in vec2 vertexPosition;	// Attrib Array 0

	out vec2 texcoord;

	void main() {
		texcoord = (vertexPosition + vec2(1, 1))/2;							// -1,1 to 0,1
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 1); 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char *fragmentSource = R"(
	#version 330
    precision highp float;

	uniform sampler2D textureUnit;
	in  vec2 texcoord;			// interpolated texture coordinates

	out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

	void main() {
		fragmentColor = texture(textureUnit, texcoord); 
	}
)";


struct vec4 {
	float v[4];

	explicit vec4(float x = 0, float y = 0, float z = 0, float w = 1) {
		v[0] = x; v[1] = y; v[2] = z; v[3] = w;
	}
};


// handle of the shader program
unsigned int shaderProgram;

class FullScreenTexturedQuad {
	unsigned int vao, textureId;	// vertex array object id and texture id
public:
	void Create(vec3 image[windowWidth * windowHeight]) {
		glGenVertexArrays(1, &vao);	// create 1 vertex array object
		glBindVertexArray(vao);		// make it active

		unsigned int vbo;		// vertex buffer objects
		glGenBuffers(1, &vbo);	// Generate 1 vertex buffer objects

								// vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo); // make it active, it is an array
		static float vertexCoords[] = { -1, -1, 1, -1, -1, 1,
			1, -1, 1, 1, -1, 1 };	// two triangles forming a quad
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexCoords), vertexCoords, GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified 
																							   // Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);     // stride and offset: it is tightly packed

																	  // Create objects by setting up their vertex data on the GPU
		glGenTextures(1, &textureId);  				// id generation
		glBindTexture(GL_TEXTURE_2D, textureId);    // binding

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, windowWidth, windowHeight, 0, GL_RGB, GL_FLOAT, image); // To GPU
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // sampling
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	void Draw() {
		glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
		int location = glGetUniformLocation(shaderProgram, "textureUnit");
		if (location >= 0) {
			glUniform1i(location, 0);		// texture sampling unit is TEXTURE0
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureId);	// connect the texture to the sampler
		}
		glDrawArrays(GL_TRIANGLES, 0, 6);	// draw two triangles forming a quad
	}
};

// The virtual world: single quad
FullScreenTexturedQuad fullScreenTexturedQuad;

vec3 background[windowWidth * windowHeight];	// The image, which stores the ray tracing result 

int sign(float num) {
	return num > 0 ? 1 : -1;
}


class Material {
	vec3   F0, kd, ks;
	float  n, shininess;
	bool reflective, refractive, rough, smk;
public:
	vec3 ka, sigma;
	Material(vec3 tores = vec3(), vec3 k = vec3(), vec3 sigma = vec3(0,0,0), vec3 kd = vec3(0, 0, 0), vec3 ks = vec3(0, 0, 0), float shininess = 0)
		:kd(kd), ks(ks), shininess(shininess), sigma(sigma)
	{
		F0 = (((tores + -1.0)*(tores + -1.0)+k*k) / ((tores + 1.0)*(tores + 1.0)+k*k));
		n = tores.x;
		reflective = refractive = rough = smk = false;
		ka = kd*(1 / 3.141592);
	}

	void setReflective(bool b)
	{
		reflective = b;
	}
	void setRefractive(bool b)
	{
		refractive = b;
	}
	void setRough(bool b)
	{
		rough = b;
	}
	void setSmoke(bool b)
	{
		smk = b;
	}
	bool isReflective() { return reflective; }
	bool isRefractive() { return refractive; }
	bool isRough() { return rough; }
	bool isSmoke() { return smk; }

	vec3 reflect(vec3 inDir, vec3 normal) {
		return inDir - normal * dot(normal, inDir) * 2.0f;
	}
	vec3 refract(vec3 inDir, vec3 normal) {
		float ior = n;
		float cosa = -dot(normal, inDir);
		if (cosa < 0) { cosa = -cosa; normal = -normal; ior = 1 / n; }
		float disc = 1 - (1 - cosa * cosa) / ior / ior;
		if (disc < 0) return reflect(inDir, normal);
		return (inDir * (1 / ior) + normal * (cosa / ior - sqrt(disc)));
	}
	vec3 shade(vec3 normal, vec3 viewDir, vec3 lightDir, vec3 inRad)
	{
		vec3 reflRad(0, 0, 0);
		float cosTheta = dot(normal, lightDir);
		if (cosTheta < 0) return reflRad;
		reflRad = inRad * kd * cosTheta;;
		vec3 halfway = (viewDir + lightDir).normalize();
		float cosDelta = dot(normal, halfway);
		if (cosDelta < 0) return reflRad;
		return reflRad + inRad * ks * pow(cosDelta, shininess);
	}

	vec3 Fresnel(vec3 inDir, vec3 normal) {
		float cosa = fabs(dot(normal, inDir));
		return F0 + (vec3(1, 1, 1) - F0) * pow(1 - cosa, 5);
	}

};
class Ray
{
public:
	Ray(vec3 p0, vec3 v) : p0(p0), v(v) {}
	vec3 p0;
	vec3 v;
};
class Camera
{
	vec3 eye, lookat, up, right, dir;
	float XM, YM;
public:
	Camera(const vec3& eye, const vec3& lookat) : eye(eye), lookat(lookat)
	{
		up = vec3(lookat.x, lookat.y + 1, lookat.z);
		dir = (lookat - eye).normalize();
		right = cross(up, dir).normalize();
		up = cross(dir, right).normalize();
		XM = windowWidth;
		YM = windowHeight;
	}
	Ray GetRay(int x, int y)
	{
		vec3 p = lookat + right*(2 * float(x) / XM - 1) + up*(2 * float(y) / YM - 1);
		return Ray(eye, (p - eye).normalize());
	}
	vec3 getLookat() { return lookat; }
};
Camera camera(vec3(0.0f, 0.5f, 0.0f), vec3(0.0f, 0.5f, 1.0f));
struct Hit {
	float t;
	vec3 position;
	vec3 normal;
	Material* material;
	Hit() { t = -1; }
};
struct Intersectable
{
	virtual Hit intersect(const Ray& ray) = 0;
};

class Sphere : public Intersectable {
	Material* material;
	vec3 center;
	float radius;
public:
	Sphere(vec3 center, float radius, Material* material) : center(center), radius(radius), material(material) {
	}

	Hit intersect(const Ray& ray) {
		float  a = dot(ray.v, ray.v);
		float b = 2 * dot((ray.p0 - center), ray.v);
		float c = dot(ray.p0 - center, ray.p0 - center) - radius*radius;

		float m1 = (-b + sqrt(b*b - 4 * a*c)) / (2 * a);
		float m2 = (-b - sqrt(b*b - 4 * a*c)) / (2 * a);

		float t = -1;
		if (m1 > 0 && m2 >0) {
			if (m2 < m1) {
				t = m2;
			}
			else {
				t = m1;
			}
		}
		else if (m1 < 0 && m2 > 0) {
			t = m2;
		}
		else if (m1 > 0 && m2 < 0) {
			t = m1;
		}

		vec3 metsz = ray.p0 + ray.v*t;
		vec3 normal = (metsz - center) *(1 / radius);

		Hit hit;
		hit.t = t;
		hit.position = metsz;

		hit.normal = normal;
		hit.material = material;

		return hit;
	}
};
class Plane : public Intersectable
{
public:
	vec3 point;
	vec3 normal;

	Material* material;

	Plane(vec3 p, vec3 n, Material* material)
	{
		point = p;
		normal = n;
		this->material = material;
	}
	Hit intersect(const Ray& ray)
	{
		Hit hit;

		float t = -dot(normal, ray.p0 - point)*(1 / (dot(normal, ray.v)));

		hit.t = t;
		hit.material = material;
		hit.normal = normal;
		hit.position = ray.p0 + ray.v*t;
		return hit;
	}

};
class Cylinder : public Intersectable {
	Material* material;
	vec3 p0;
	vec3 v;
	float radius;
public:
	Cylinder(vec3 p0, vec3 v, float radius, Material* material)
		: p0(p0), radius(radius), material(material) {
		this->v = v.normalize();
	}

	Hit intersect(const Ray& ray) {
		float  a = dot(ray.v, ray.v) - dot(ray.v, v)*dot(ray.v, v);
		float b = 2 * dot(ray.p0 - p0, ray.v) - 2 * dot(ray.p0 - p0, v)*dot(ray.v, v);
		float c = dot(ray.p0 - p0, ray.p0 - p0) - dot(ray.p0 - p0, v)*dot(ray.p0 - p0, v) - radius*radius;

		float m1 = (-b + sqrt(b*b - 4 * a*c)) / (2 * a);
		float m2 = (-b - sqrt(b*b - 4 * a*c)) / (2 * a);
		float t = -1;
		if (m1 > 0 && m2 >0) {
			if (m2 < m1) {
				t = m2;
			}
			else {
				t = m1;
			}
		}
		else if (m1 < 0 && m2 > 0) {
			t = m2;
		}
		else if (m1 > 0 && m2 < 0) {
			t = m1;
		}
		vec3 metsz = ray.p0 + ray.v*t;
		vec3 normal = (((metsz - p0) - v*dot(metsz - p0, v))*(1 / radius));
		normal = normal*sign(dot(-ray.v, normal));
		Hit hit;
		hit.t = t;
		hit.position = metsz;
		hit.normal = normal;
		hit.material = material;

		return hit;
	}
};

struct Light {
	vec3 position;
	vec3 Lout;

	vec3 getL(vec3 inPos)
	{
		return Lout* (1 / ((position - inPos).Length()*(position - inPos).Length()));
	}
	Light() {}
	Light(vec3 position, vec3 Lout) : Lout(Lout), position(position) {}
};


class Scene
{
	int maxdepth;
	Intersectable* objects[30];
	int siz;
	Light lights[10];
	int lightnum;
	float epsilon;
public:

	Scene()
	{
		siz = 0;
		maxdepth = 10;
		lightnum = 0;
		epsilon = 0.001;
	}
	~Scene()
	{
		for (int i = 0; i <siz; i++) {
			delete objects[i];
		}
	}

	void addObject(Intersectable* object)
	{
		objects[siz++] = object;
	}
	void addLight(Light light)
	{
		lights[lightnum++] = light;
	}
	Hit firstIntersect(Ray ray) {
		Hit bestHit;
		for (int i = 0; i <siz; i++) {
			Hit hit = objects[i]->intersect(ray);
			if (hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t)) 	bestHit = hit;
		}
		return bestHit;
	}

	vec3 sigma(vec3 pos)
	{

		float ret = fabs(pos.x*pos.y*pos.z)+0.5;
		return vec3(ret, ret, ret).normalize()*0.4;

	}
	vec3 trace(Ray ray, int depth) {
		vec3 La(0.1, 0.1, 0.2);
		if (depth > maxdepth) return La;
		Hit hit = firstIntersect(ray);
		if (hit.t < 0) return La;
		vec3 outRadiance;
		if (hit.material->isRough()) {
			outRadiance = hit.material->ka*La;
			for (int i = 0; i < lightnum; i++) {
				Ray shadowRay(hit.position + hit.normal*epsilon*sign(dot(hit.normal, -ray.v)), (lights[i].position - hit.position).normalize());
				Hit shadowHit = firstIntersect(shadowRay);
				if (shadowHit.t < 0 || shadowHit.t >(lights[i].position - hit.position).Length()) {
					outRadiance = outRadiance + hit.material->shade(hit.normal, -ray.v, shadowRay.v, lights[i].getL(hit.position));
					if (fmodf(fabs(hit.position.x + hit.position.y +hit.position.z)+0.125, 0.5) <= 0.25)
							outRadiance = outRadiance*0.6;
				}
			}
		}
		if (hit.material->isSmoke())
		{
			Ray secondRay(hit.position + ray.v*epsilon*epsilon, ray.v);
			Hit secondHit = firstIntersect(secondRay);
			vec3 rad;
			float delta = 0.05;
			for (float s = 0; s <= (secondHit.position - hit.position).Length(); s += delta)
			{
				rad = rad + sigma(hit.position + ray.v*s);
			}
			Ray thirdRay(secondHit.position + ray.v*epsilon, ray.v);
			outRadiance = outRadiance + trace(thirdRay, depth + 1);
			outRadiance.x = outRadiance.x*exp(-rad.x);
			outRadiance.y = outRadiance.y*exp(-rad.y);
			outRadiance.z = outRadiance.z*exp(-rad.z);
		}
		if (hit.material->isReflective()) {
			vec3 reflectionDir = hit.material->reflect(ray.v, hit.normal);
			Ray reflectedRay(hit.position + hit.normal*sign(dot(hit.normal, -ray.v))*epsilon, reflectionDir);

			outRadiance = outRadiance + trace(reflectedRay, depth + 1)*hit.material->Fresnel(-ray.v, hit.normal);
		}
		if (hit.material->isRefractive()) {
			vec3 refractionDir = hit.material->refract(ray.v, hit.normal);
			Ray refractedRay(hit.position - hit.normal*sign(dot(hit.normal, -ray.v))*epsilon, refractionDir);
			Ray secondRay(hit.position - hit.normal*sign(dot(hit.normal, -ray.v))*epsilon, refractionDir);
			Hit secondHit = firstIntersect(secondRay);
			outRadiance = outRadiance + trace(refractedRay, depth + 1)*(vec3(1, 1, 1) - hit.material->Fresnel(-ray.v, hit.normal));
			outRadiance.x = outRadiance.x*(powf(1 - hit.material->sigma.x, (hit.position - secondHit.position).Length()));
			outRadiance.y = outRadiance.y*(powf(1 - hit.material->sigma.y, (hit.position - secondHit.position).Length()));
			outRadiance.z = outRadiance.z*(powf(1 - hit.material->sigma.z, (hit.position - secondHit.position).Length()));
		}
		return outRadiance;
	}
};
Scene scene;

// Initialization, create an OpenGL context
void onInitialization() {
	glViewport(0, 0, windowWidth, windowHeight);

	scene.addLight(Light(vec3(-0.5f, 0.7f, 1.0f), vec3(0.5, 0.5, 0.5)));
	scene.addLight(Light(vec3(1.0f, 0.5f, 0.0f), vec3(0.5, 0.5, 0.5)));
	scene.addLight(Light(vec3(0.0f, 0.5f, 0.0f), vec3(0.5, 0.5, 0.5)));

	Material goldMaterial(vec3(0.17, 0.35, 1.5), vec3(3.1, 2.7, 1.9));
	goldMaterial.setReflective(true);

	Material glassMaterial(vec3(1.5, 1.5, 1.5), vec3(0, 0, 0), vec3(0.1, 0.1, 0.1));
	glassMaterial.setRefractive(true);

	Material roughMaterial(vec3(0, 0, 0), vec3(0, 0, 0), vec3(0,0,0), vec3(1.0, 25.0/255, 100.0/255), vec3(1, 1, 1), 50);
	roughMaterial.setRough(true);

	Material roughMaterial2(vec3(0, 0, 0), vec3(0, 0, 0), vec3(0, 0, 0), vec3(0.7, 0.7, 0.7), vec3(1, 1, 1), 50);
	roughMaterial2.setRough(true);

	Material silverMaterial(vec3(0.14, 0.16, 0.13), vec3(4.1, 2.3, 3.1));
	silverMaterial.setReflective(true);

	Material beerMaterial(vec3(1.33, 1.33, 1.33), vec3(), vec3(0.1, 0.1, 0.5));
	beerMaterial.setRefractive(true);

	Material bubbleMaterial(vec3(1.0 / 1.33, 1.0 / 1.33, 1.0 / 1.33), vec3());
	bubbleMaterial.setRefractive(true);

	Material smokeMaterial;
	smokeMaterial.setSmoke(true);



	scene.addObject(new Cylinder(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f), 3.0f, &goldMaterial));
	scene.addObject(new Plane(vec3(0, 0, 0), vec3(0, 1, 0), &roughMaterial2));
	scene.addObject(new Plane(vec3(0, 1, 0), vec3(0, -1, 0), &roughMaterial2));

	scene.addObject(new Cylinder(vec3(0.2f, 0.0f, 1.0f), vec3(0.25f, 1.0f, 0.5f).normalize(), 0.1f, &silverMaterial));
	scene.addObject(new Cylinder(vec3(-0.4f, 0.0f, 1.1f), vec3(-0.6f, 1.0f, 0.0f).normalize(), 0.1f, &glassMaterial));
	scene.addObject(new Cylinder(vec3(0.3f, 0.3f, 0.5), vec3(-0.6f, 0.7f, 2.0f).normalize(), 0.1f, &beerMaterial));
	scene.addObject(new Cylinder(vec3(-1.2f, 0.1f, 1.45f), vec3(0.4f, 1.0f, 0.3f).normalize(), 0.1f, &roughMaterial));
	scene.addObject(new Cylinder(vec3(0.7f, 0.5f, 0.9f), vec3(0.5, 1, 0).normalize(), 0.1f, &smokeMaterial));
	
	//buborekok
	scene.addObject(new Sphere(vec3(0.3f, 0.32f, 0.5f), 0.015f, &bubbleMaterial));
	scene.addObject(new Sphere(vec3(0.32f, 0.35f, 0.55f), 0.015f, &bubbleMaterial));
	scene.addObject(new Sphere(vec3(0.3f, 0.32, 0.6f), 0.015f, &bubbleMaterial));
	scene.addObject(new Sphere(vec3(0.22f, 0.35, 0.65f), 0.015f, &bubbleMaterial));
	scene.addObject(new Sphere(vec3(0.28f, 0.38, 0.7f), 0.015f, &bubbleMaterial));




	// Ray tracing fills the image called background
	for (int x = 0; x < windowWidth; x++) {
		for (int y = 0; y < windowHeight; y++) {
			background[y * windowWidth + x] = scene.trace(camera.GetRay(x, y), 0);
		}
	}

	fullScreenTexturedQuad.Create(background);

	// Create vertex shader from string
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (!vertexShader) {
		printf("Error in vertex shader creation\n");
		exit(1);
	}
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	checkShader(vertexShader, "Vertex shader error");

	// Create fragment shader from string
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (!fragmentShader) {
		printf("Error in fragment shader creation\n");
		exit(1);
	}
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	checkShader(fragmentShader, "Fragment shader error");

	// Attach shaders to a single program
	shaderProgram = glCreateProgram();
	if (!shaderProgram) {
		printf("Error in shader program creation\n");
		exit(1);
	}
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Connect the fragmentColor to the frame buffer memory
	glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

																// program packaging
	glLinkProgram(shaderProgram);
	checkLinking(shaderProgram);
	// make this program run
	glUseProgram(shaderProgram);
}

void onExit() {
	glDeleteProgram(shaderProgram);
	printf("exit");
}

// Window has become invalid: Redraw
void onDisplay() {
	fullScreenTexturedQuad.Draw();
	glutSwapBuffers();									// exchange the two buffers
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY) {
	if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY) {

}

// Mouse click event
void onMouse(int button, int state, int pX, int pY) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
	}
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY) {
}

// Idle event indicating that some time elapsed: do animation here
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
}

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(majorVersion, minorVersion);
#endif
	glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
	glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_2_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif

	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	onInitialization();

	glutDisplayFunc(onDisplay);                // Register event handlers
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();
	onExit();
	return 1;
}